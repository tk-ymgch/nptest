﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StimulusManager : MonoBehaviour
{

    private SpatialSpanTaskManager _taskMCtr;
    private SoundManager _soundMgr;
    private static int _currentPressNum; //刺激を何回選択したか
    private static int _currentCorrectNum; //何回成功したか
    private int[] _currentTaskPattern;
    private int _currentTrialLimit;
    private int _currentTrial;
    // Start is called before the first frame update
    void Start()
    {
        InitializeParameters();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        _currentPressNum++;
        Debug.Log("mouse down:" + this.gameObject.name);

        if (_currentCorrectNum != _currentTrialLimit)
        {
            bool flag = CheckAnswer(_currentPressNum);

            if (flag)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = _taskMCtr._stimulus[1];
                DatabaseManager.Instance.evt = convertOBJNameToEVENT() + "_IS_CORRECT";
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = _taskMCtr._stimulus[2];
                _soundMgr.PlayBeepSound();
                DatabaseManager.Instance.evt = convertOBJNameToEVENT() + "_IS_MISS";
                Invoke("ResetStimulus", 0.25f);
            }
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = _taskMCtr._stimulus[2];
            _soundMgr.PlayBeepSound();
            DatabaseManager.Instance.evt = convertOBJNameToEVENT() + "_IS_MISS";
            Invoke("ResetStimulus", 0.25f);
        }
    }

    //不正解判定時の色を元に戻す
    private void ResetStimulus()
    {
        if(CheckPressedButtonIsAnswer())
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = _taskMCtr._stimulus[1];
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = _taskMCtr._stimulus[0];
        }


    }

    //クリックされたオブジェクトが_currentTaskPatternの中の正解と一致しているかどうか
    private bool CheckPressedButtonIsAnswer()
    {
        bool flag = false;

        for(int i = 0; i < _currentTrialLimit; i++)
        {
            string [] buttonNumber = this.gameObject.name.Split("stimulus"[0]);

            if (_currentTaskPattern[i] == int.Parse(buttonNumber[2]))
            {                
                return true;
            }

        }

        return flag;

    }
    private void InitializeParameters()
    {
        _taskMCtr = GameObject.Find("TaskManager").GetComponent<SpatialSpanTaskManager>();
        _soundMgr = GameObject.Find("UIManager").GetComponent<SoundManager>();

        _currentCorrectNum = 0;
        _currentTrialLimit = _taskMCtr.currentTrialLimit;
        _currentTrial = _taskMCtr.currentTrial;
        _currentTaskPattern = new int[_currentTrialLimit];
        for (int i = 0; i < _currentTrialLimit; i++)
        {
            _currentTaskPattern[i] = _taskMCtr.taskPatterns[_currentTrial-1, i];
        }
        _currentPressNum = 0;
    }

    //選択した刺激の正解をチェック
    private bool CheckAnswer(int currentPressNum)
    {
        bool flag = false;

        
        if (_currentCorrectNum != _currentTrialLimit)
        {
            Debug.Log("【Answer】:" + _currentTaskPattern[_currentCorrectNum] + "【pressed 】:" + this.gameObject.name + " 【num of pressed】: " + _currentPressNum);

            if ("stimulus" + _currentTaskPattern[_currentCorrectNum] == this.gameObject.name)
            {
                Debug.Log("Correct!");
                _currentCorrectNum++;
                flag = true;
            }
            else
            {
                flag = false;
                Debug.Log("Miss!");
            }
        }

        return flag;
    }

    private string convertOBJNameToEVENT()
    {
        string evtName = "";

        switch (this.gameObject.name)
        {
            case "stimulus0":
                evtName = EVENT.S0_OBJ_PRESSED.ToString();
                break;
            case "stimulus1":
                evtName = EVENT.S1_OBJ_PRESSED.ToString();
                break;
            case "stimulus2":
                evtName = EVENT.S2_OBJ_PRESSED.ToString();
                break;
            case "stimulus3":
                evtName = EVENT.S3_OBJ_PRESSED.ToString();
                break;
            case "stimulus4":
                evtName = EVENT.S4_OBJ_PRESSED.ToString();
                break;
            case "stimulus5":
                evtName = EVENT.S5_OBJ_PRESSED.ToString();
                break;
            case "stimulus6":
                evtName = EVENT.S6_OBJ_PRESSED.ToString();
                break;
            case "stimulus7":
                evtName = EVENT.S7_OBJ_PRESSED.ToString();
                break;
            case "stimulus8":
                evtName = EVENT.S8_OBJ_PRESSED.ToString();
                break;
            case "stimulus9":
                evtName = EVENT.S9_OBJ_PRESSED.ToString();
                break;
        }


        Debug.Log(evtName + "が送信されました");

        return evtName;
    }

}
