﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GobackToStartButtonCallback : MonoBehaviour
{
    /// ボタンをクリックした時の処理
    public void OnClick()
    {
        Debug.Log("Button click!");
        SceneManager.LoadScene("main");
    }

}
