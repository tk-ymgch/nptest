﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public abstract class TaskManagerCtr : MonoBehaviour
{

    public abstract void OnTaskBegin();//カウントダウン後に呼ばれる。タスク開始後最初に呼ばれる
    protected abstract void DisplayTaskEndButton();//タスクを終了するときに用いるボタンを表示
    protected abstract void DeleteTaskEndButton();//TaskEndButtonを非表示
    public abstract void OnTaskEndButton();//TaskEndButtonが押された時のイベントハンドラ
    protected abstract void DisplayGobackToMenuButton();//メニューに戻るボタンを表示
    public abstract void LoadTaskPatterns();//タスクパターンをロード
    protected abstract void LoadTargets();//タスクに用いる刺激をロード


    /**
        private static int MAX_NUM_OF_STIMULUS = 10;
        private int _currentTrial = 0;
        private int _currentTrialLimit = 3;
        public Sprite[] _stimulus;
        private int[,] _taskPatterns;
        public GameObject[] _stimulusObj;
        public float _intervalTime = 1.0f;
        private int _currentOrder = 0;
        private GameObject _taskEndBtn;
        public GameObject _canvas;//キャンバス
        public GameObject _Button;

        //  カウントダウン終了後に呼ばれる
        public void OnTaskBegin()
        {
            Debug.Log("TaskManagerCtr");
            LoadTargets();
            LoadTaskPatterns();
            _currentTrialLimit = 3;//3から始めて１個ずつ10まで増える
            _currentTrial = 0;
            _currentOrder = 0;
            DatabaseManager.Instance.maxTrialNum = 8;
        }

        //　タスク中に表示される終了ボタンのイベント
        public void OnTaskEndButton()
        {
            Debug.Log("OnTaskEndButton:タスクが終了しました");
           
            DatabaseManager.Instance.EndLog();//データログ停止
            DeleteAllStimulus();
            DeleteTaskEndButton();

            if (_currentTrial != 8)//8タスク
            {
                _currentOrder = 0;
                //すべてのディストラクタ刺激を表示する
                SetAllStimulusForTaskInstruction();
                //順番を追ってターゲット刺激を表示する
                InvokeRepeating("PutTargetStimulus", 1.0f, _intervalTime);
            }
            else
            {
                DisplayGobackToMenuButton();
                DatabaseManager.Instance.EndLogAndExport();//データログ停止 & 保存

            }

        }

        void DeleteTaskEndButton()
        {

            Debug.Log("DeleteTaskEndButton:TaskEndButtonを消しました");
            GameObject.Find("TaskEndButton").SetActive(false);
        }

        void DisplayGobackToMenuButton()
        {
            Debug.Log("DisplayGobackToMenuButton:GobackToMenuButtonを表示");

            Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();// Canvasコンポーネントを保持

            foreach (Transform child in _canvas.transform)
            {
                Debug.Log(child.name);
                // 子の要素をたどる
                if (child.name == "GobackToMenuButton")
                {
                    // 指定した名前と一致
                    // 表示フラグを設定
                    child.gameObject.SetActive(true);
                    // おしまい

                }
            }
        }

        //刺激パターンの読み込み
        public void LoadTaskPatterns()
        {
            Debug.Log("LoadTaskPatterns:タスクパターンをロード");

            StreamReader sr = new StreamReader("./Assets/Resources/SpatialSpan/TaskPattern/taskPattern.txt", System.Text.Encoding.GetEncoding("shift_jis"));
            string fileContents = sr.ReadToEnd();
            sr.Close();

            _taskPatterns = new int[8, 10];

            var lines = fileContents.Split("\n"[0]);
            for (int j = 0; j < lines.Length; j++)
            {
                var patterns = lines[j].Split(","[0]);
                for (int k = 0; k < 10; k++)
                {
                    _taskPatterns[j, k] = int.Parse(patterns[k]);
                }

            }
        }

        //刺激画像の読み込み
        void LoadTargets()
        {
            Debug.Log("LoadTargets:ターゲット刺激を読みこむ");
            //spriteの読み込み（multiple）
            _stimulus = Resources.LoadAll<Sprite>("SpatialSpan/target");

            //spriteを表示するためのゲームオブジェクトの配列
            _stimulusObj = new GameObject[MAX_NUM_OF_STIMULUS];

            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                _stimulusObj[i] = new GameObject();
                _stimulusObj[i].gameObject.name = "stimulus" + i.ToString();
            }

            _stimulusObj[0].transform.position = new Vector3(3.47f, 1.28f, 0.5f);
            _stimulusObj[1].transform.position = new Vector3(-3.63f, -3.4f, 0.5f);
            _stimulusObj[2].transform.position = new Vector3(-3.67f, 3.46f, 0.5f);
            _stimulusObj[3].transform.position = new Vector3(-0.91f, 2.3f, 0.5f);
            _stimulusObj[4].transform.position = new Vector3(1.99f, -2.78f, 0.5f);
            _stimulusObj[5].transform.position = new Vector3(3.47f, 3.95f, 0.5f);
            _stimulusObj[6].transform.position = new Vector3(-6.51f, -0.33f, 0.5f);
            _stimulusObj[7].transform.position = new Vector3(6.7f, 2.37f, 0.5f);
            _stimulusObj[8].transform.position = new Vector3(5.79f, -1.71f, 0.5f);
            _stimulusObj[9].transform.position = new Vector3(-1.67f, -0.56f, 0.5f);

            //すべてのディストラクタ刺激を表示する
            PutAllStimulus();
            //順番を追ってターゲット刺激を表示する
            InvokeRepeating("PutTargetStimulus", 0.0f, _intervalTime);
        }

        void DisplayTaskEndButton()
        {
            Debug.Log("DisplayTaskEndButton:TaskEndButtonを表示");
            float scrWidth = Screen.width;
            float scrHeight = Screen.height;

           
            _taskEndBtn = (GameObject)Instantiate(_Button);
            _taskEndBtn.transform.SetParent(_canvas.transform, false);
            _taskEndBtn.gameObject.name = "TaskEndButton";
            _taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(-scrWidth / 2 + 160 / 2 + 15, scrHeight / 2 - 30, 0);
            _taskEndBtn.GetComponent<Button>().onClick.AddListener(OnTaskEndButton);
            //_taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(0,0, 0);
        }//

        void PutTargetStimulus()
        {
            Debug.Log("PutTargetStimulus:刺激を表示");
            //MAX_NUM_OF_STIMULUS数表示されたら表示をやめる
            if (_currentOrder == _currentTrialLimit)
            {
                _currentTrial++; DatabaseManager.Instance.trialNum++;
                _currentTrialLimit++;
                CancelInvoke("PutTargetStimulus");
                DeleteAllStimulus();
                Invoke("SetAllStimulusForTask", 1.0f);//タスク用の刺激を表示する

                return;
            }


            int targetStimuNum = _taskPatterns[_currentTrial, _currentOrder];

            _stimulusObj[targetStimuNum].GetComponent<SpriteRenderer>().sprite = _stimulus[1];
            //_stimulusObj[_currentOrder].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

            //ターゲット以外は白にする
            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                if (i != targetStimuNum)
                {
                    //_stimulusObj[i].AddComponent<SpriteRenderer>();
                    _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
                    _stimulusObj[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                }
            }

            _currentOrder++;

        }

        void PutAllStimulus()
        {
            Debug.Log("PutAllStimulus:");

            if (_stimulusObj[0].GetComponent<SpriteRenderer>() == null)
            {
                for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
                {
                    _stimulusObj[i].AddComponent<SpriteRenderer>();
                    _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
                    _stimulusObj[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    _stimulusObj[i].AddComponent<CircleCollider2D>();
                    _stimulusObj[i].AddComponent<StimulusManager>();

                }
            }

        }

        void DeleteAllStimulus()
        {
            Debug.Log("DeleteAllStimulus:");

            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                _stimulusObj[i].gameObject.SetActive(false);
            }
        }

        void SetAllStimulusForTaskInstruction()
        {
            Debug.Log("SetAllStimulusForTaskInstruction:");

            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                _stimulusObj[i].gameObject.SetActive(true);
                _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
            }
            Debug.Log("トライアルスタート");

        }

        void SetAllStimulusForTask()
        {
            Debug.Log("SetAllStimulusForTask:");

            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                _stimulusObj[i].gameObject.SetActive(true);
                _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
            }
            Debug.Log("トライアルスタート");
            DisplayTaskEndButton();//taskEndButtonの表示
            DatabaseManager.Instance.cTaskStatus = CURRENT_TASK_STATUS.SPATIAL_SPAN;
            DatabaseManager.Instance.StartLog();//データログ開始
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                DatabaseManager.Instance.EndLog();//データログ停止
            }
        }
        **/
}
