﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    static Canvas _canvas;

    public void ChangeEnabled()
    {
        _canvas = GameObject.Find("Canvas").GetComponent<Canvas>();// Canvasコンポーネントを保持
        SetActive(false);//Buttonを非表示にする

    }

    /// 表示・非表示を設定する
    public static void SetActive(bool b)
    {
        

        foreach (Transform child in _canvas.transform)
        {
            Debug.Log(child.name);
            // 子の要素をたどる
            if (child.name == "GobackToMenuButton" || child.name == "StartButton")
            {
                // 指定した名前と一致
                // 表示フラグを設定
                child.gameObject.SetActive(b);
                // おしまい
                
            }
        }
        
    }
}
