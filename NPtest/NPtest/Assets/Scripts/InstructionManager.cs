﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class InstructionManager : MonoBehaviour
{
    public Sprite _instructionWindow;
    public GameObject _instructionWindowObj;
    private string _instructionSentence;
    

    // Start is called before the first frame update
    public void OnClick()
    {
        Debug.Log("Instruction");
        LoadInstruction();
        InitializeInstructionWindow();
        DisplayInstruction();
    }

    void LoadInstruction()
    {
        StreamReader sr = new StreamReader("./Assets/Resources/SpatialSpan/Instructions/instruction.txt", System.Text.Encoding.GetEncoding("shift_jis"));
        string fileContents = sr.ReadToEnd();
        sr.Close();


        _instructionSentence = fileContents;

        var lines = fileContents.Split("\n"[0]);
        for (int j = 0; j < lines.Length; j++)
        {
            Debug.Log(lines[j]);
        }
    }

    void DisplayInstruction()
    {
       // _instructionSentence
    }

    void InitializeInstructionWindow()
    {

        //spriteの読み込み（multiple）
        _instructionWindow = Resources.Load<Sprite>("SpatialSpan/instruction");

        //spriteを表示するためのゲームオブジェクトの配列
        _instructionWindowObj = new GameObject();
        _instructionWindowObj.AddComponent<SpriteRenderer>();
        _instructionWindowObj.GetComponent<SpriteRenderer>().sprite = _instructionWindow;
        _instructionWindowObj.transform.position = new Vector3(0, 3.53f, 0);
 //       _instructionWindowObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        
    }



}
