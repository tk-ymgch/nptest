﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayoutManager : MonoBehaviour
{
    private float _scrWidth;
    private float _scrHeight;

    private void Awake()
    {
        _scrWidth = Screen.width;
        _scrHeight = Screen.height;

    }
    // Start is called before the first frame update
    void Start()
    {
        //1924,911
        Debug.Log(_scrWidth + ","+_scrHeight);
        GameObject.Find("StartButton").GetComponent<RectTransform>().localPosition = new Vector3(_scrWidth/2-160*2+20, -(_scrHeight/2 - 30-20), 0);
        GameObject.Find("GobackToMenuButton").GetComponent<RectTransform>().localPosition = new Vector3(_scrWidth / 2 - 160 + 30, -(_scrHeight / 2 - 30 - 20), 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
