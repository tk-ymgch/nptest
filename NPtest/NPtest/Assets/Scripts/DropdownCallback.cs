﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum scneID {

    SPATIAL_SPAN = 0,
    VERBAL_LEARNING_TEST,
    DIGIT_SYMBOL_TEST,
    N_BACK_TEST,
    FINE_MOTOR_TEST,
    TMT_A,
    TMT_B,
    STROOP_TEST,
    SPATIAL_SPAN_REV
}

public class DropdownCallback : MonoBehaviour {

    public void OnValueChanged(int result)
    {
       
        Dropdown dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();
        int v = dropdown.value;
        // 処理
        Debug.Log(v);

        scneID sID = (scneID)v;

        switch (sID)
        {

            case scneID.SPATIAL_SPAN://Spatial Spanシーンへ
                SceneManager.LoadScene("SpatialSpan");
                break;
            case scneID.VERBAL_LEARNING_TEST://Verval learning testシーンへ
                SceneManager.LoadScene("VerbalLearningTest");
                break;

            case scneID.DIGIT_SYMBOL_TEST://Verval learning testシーンへ
                SceneManager.LoadScene("DigitSymbolTest");
                break;

            case scneID.N_BACK_TEST://N back testシーンへ
                SceneManager.LoadScene("NBackTest");
                break;
            case scneID.FINE_MOTOR_TEST://FineMotor シーンへ
                SceneManager.LoadScene("FineMotorTest");
                break;
            case scneID.TMT_A://TMT Aシーンへ
                SceneManager.LoadScene("TMTA");
                break;
            case scneID.TMT_B://TMT Bシーンへ
                SceneManager.LoadScene("TMTB");
                break;
            case scneID.STROOP_TEST://Stroop testシーンへ
                SceneManager.LoadScene("StroopTest");
                break;
            case scneID.SPATIAL_SPAN_REV://Spatial Span Revシーンへ
                SceneManager.LoadScene("SpatialSpanRev");
                break;

        }
    }
}
