﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextManager : MonoBehaviour
{
   public void ChangeEnabled(Text TextComponent)
    {
        TextComponent.enabled = !TextComponent.enabled;
    }
}
