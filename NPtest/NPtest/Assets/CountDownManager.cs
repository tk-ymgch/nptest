﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CountDownManager : MonoBehaviour
{
    public Sprite[] _cdSprite;
    public GameObject[] _cdObj;
    private int _currentCount = 3;

    public UnityEvent ON_COUNTDOWN_FINISH;//UnityEventをpublicで作る

    // Use this for initialization
    void Awake()
    {
        if (ON_COUNTDOWN_FINISH == null)
        {
            ON_COUNTDOWN_FINISH = new UnityEvent();
        }
    }

    //UnityEventに割り当てたいイベントを作成　
  


    public void OnCountDownBegin()
    {     

        Initialize();

    }
    void Initialize()
    {

        _cdSprite = new Sprite[3];
        Debug.Log("Count down begin!");
        //spriteの読み込み（multiple）
        _cdSprite[0] = Resources.Load<Sprite>("CountDown/count1");
        _cdSprite[1] = Resources.Load<Sprite>("CountDown/count2");
        _cdSprite[2] = Resources.Load<Sprite>("CountDown/count3");

        _cdObj = new GameObject[_cdSprite.Length];

        InvokeRepeating("CountDown", 0.0f, 1.0f);

    }

    void CountDown()
    {
        if (_currentCount == 0)
        {
            Destroy(_cdObj[_currentCount]);//前に表示されているオブジェクトを消去
            CancelInvoke("CountDown");

            if (ON_COUNTDOWN_FINISH != null)
                ON_COUNTDOWN_FINISH.Invoke();

            return;
        }


        if (_currentCount != 3)
            Destroy(_cdObj[_currentCount]);//前に表示されているオブジェクトを消去

        _cdObj[_currentCount - 1] = new GameObject();
        _cdObj[_currentCount - 1].AddComponent<SpriteRenderer>();
        _cdObj[_currentCount - 1].GetComponent<SpriteRenderer>().sprite = _cdSprite[_currentCount - 1];
        _cdObj[_currentCount - 1].transform.localPosition = new Vector3(0, 0, 0);

        _currentCount--;


    }
}
