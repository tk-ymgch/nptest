﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;

public enum CURRENT_TASK_STATUS
{
    SPATIAL_SPAN = 0
}

public enum EVENT
{
    TRIAL_BEGIN = 0,
    TRIAL_END,
    S0_OBJ_PRESSED,
    S1_OBJ_PRESSED,
    S2_OBJ_PRESSED,
    S3_OBJ_PRESSED,
    S4_OBJ_PRESSED,
    S5_OBJ_PRESSED,
    S6_OBJ_PRESSED,
    S7_OBJ_PRESSED,
    S8_OBJ_PRESSED,
    S9_OBJ_PRESSED
}

public class DatabaseManager : MonoBehaviour
{
    public GameObject _canvas;//キャンバス
    private GameObject _debugWindow;
    public GameObject _debugWindowPrefab;
    private StreamWriter _outputFile;
    private float _time;
    private bool _debugFlag = false;
    private bool _beginFlag = false;
    private StringBuilder[] _log;
    private string _debugLog = "";
    private static DatabaseManager instance = null;
    private int _maxTrialNum;
    public int maxTrialNum
    {
        set {
            _maxTrialNum = value;
            _log = new StringBuilder[_maxTrialNum];

            for(int i = 0; i < _maxTrialNum; i++)
            {
                _log[i] = new StringBuilder();
            }
        }
        get { return _maxTrialNum; } 
    }
    private int _trialNum = 0;
    public int trialNum
    {
        set { _trialNum = value; }
        get { return _trialNum; }
    }
    private int _appliedPattern;//提示された刺激のパターンを格納
    public int appliedPattern
    {
        set { _appliedPattern = value; }
        get { return _appliedPattern; }
    }
    private string _judgedAnswer;
    public string judgedAnser
    {
        set { _judgedAnswer = value; }
        get { return _judgedAnswer; }
    }

    private string _prevEvt;
    private string _evt;
    public string evt
    {
        set { _evt = value; }
        get { return _evt; } 
    }
    private CURRENT_TASK_STATUS _cTaskStatus;
    public CURRENT_TASK_STATUS cTaskStatus
    {
        set { _cTaskStatus = value; }
        get { return _cTaskStatus; }
    }
    // シングルトン化するために必要
    public static DatabaseManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("DatabaseManager").AddComponent<DatabaseManager>();
            }

            return instance;
        }
    }

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void StartLog()
    {
        InitializeParameters();
        SetDebugWindow();
        _beginFlag = true;
        
    }

    private void InitializeParameters()
    {
        _time = 0.0f;
        //_trialNum = 0;
        _evt = EVENT.TRIAL_BEGIN.ToString();
        _prevEvt = "-";
    }

    public void EndLog()
    {        
        _beginFlag = false;
        
        
    }

    public void EndLogAndExport()
    {
        _beginFlag = false;
        ExportLog();

    }

    private void ExportLog()
    {
        DateTime dt = DateTime.Now;
     
        string date = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();
        string file = Application.dataPath + "/StreamingAssets/ExportedData/" + cTaskStatus.ToString() + " /outdata_"+ date+".txt";
        _outputFile = new StreamWriter(file);

        string log = "";
        for(int i  = 0; i < _maxTrialNum; i++)
        {
            log += _log[i];
        }

        _outputFile.WriteLine(log);
        _outputFile.Flush();
        _outputFile.Close();
    }

    private void Update()
    {
        if(_beginFlag)
        {

            if (_evt == _prevEvt) _evt = "-";

            _time += Time.deltaTime;
            _log[_trialNum-1].AppendFormat("{0},{1},{2}\n", _time, _trialNum,_evt);
            _debugLog = _time.ToString() + "," + _trialNum.ToString() +","+ _evt;


            _prevEvt = _evt;
            //デバックがONなら表示
            if (_debugFlag) UpdateDebugWindow();
        }

        //デバックウィンドウの表示・非表示
        if(Input.GetKeyDown(KeyCode.D))
        {
            _debugFlag = !_debugFlag;

            if (_debugFlag) _debugWindow.SetActive(true);
            else _debugWindow.SetActive(false);
        }
    }

    void UpdateDebugWindow()
    {
        // _debugWindow.GetComponent<Text>().text = _debugLog;
        _debugWindow.GetComponent<Text>().text = Application.dataPath;

    }
    void SetDebugWindow()
    {

            float scrWidth = Screen.width;
            float scrHeight = Screen.height;

        if (_debugWindow == null)
        {
            //_canvas = GameObject.Find("Canvas");
            //Debug.Log("DisplayDebugWindow");
            _debugWindow = GameObject.Find("DebugWindow");// (GameObject)Instantiate(_debugWindowPrefab);
            //_debugWindow.transform.SetParent(_canvas.transform, false);
            //_debugWindow.gameObject.name = "DebugWindow";
            _debugWindow.GetComponent<RectTransform>().localPosition = new Vector3(-scrWidth / 2 + 200 / 2 + 30, -scrHeight / 2 + 30, 0);

            
            //_taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(0,0, 0);
        }
        
    }
}
