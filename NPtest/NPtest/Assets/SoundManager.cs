﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    private AudioClip _beepClip;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Sound Loaded");
        _beepClip = (AudioClip)Resources.Load("SpatialSpan/Sounds/beepsound");
    }

    public void PlayBeepSound()
    {
        PlayAudio(_beepClip, 1.0f);
    }

    AudioSource PlayAudio(AudioClip clip, float volume)
    {
        GameObject pAudio;

        if (!GameObject.Find("SoundManager"))
        { 
            pAudio = new GameObject("SoundManager");
        }
        else
        {
            pAudio = GameObject.Find("SoundManager");
        }
        AudioSource source = pAudio.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.Play();

        return source;
    }
}
