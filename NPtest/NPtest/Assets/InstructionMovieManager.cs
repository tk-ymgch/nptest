﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionMovieManager : MonoBehaviour
{

    //　再生する動画
    [SerializeField]
    private MovieTexture _movies;

    // Start is called before the first frame update
    void Start()
    {
        InitializeMovie();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitializeMovie()
    {
        //　最初の動画を設定
        GetComponent<MeshRenderer>().material.mainTexture = _movies;

        //　動画をループ設定
        _movies.loop = true;
    }
}
