﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// © 2018 TheFlyingKeyboard and released under MIT License 
// theflyingkeyboard.net 
public class LineCreator : MonoBehaviour
{
    [SerializeField] private GameObject line;
    private Vector2 mousePosition;
    private int _countObj = 0;

    private void Awake()
    {
        _countObj = 0;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) //Or use GetKeyDown with key defined with mouse button
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject lineObj = Instantiate(line, mousePosition, Quaternion.Euler(0.0f, 0.0f, 0.0f));
            lineObj.gameObject.name = "LineDrawer" + _countObj;
            _countObj++;
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            ClearLine();
        }
    }

    public void ClearLine()
    {
        for(int i = 0; i < _countObj; i++)
        {
            Destroy(GameObject.Find("LineDrawer" + i));
        }

        _countObj = 0;
    }
}