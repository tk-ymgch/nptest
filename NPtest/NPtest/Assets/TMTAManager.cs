﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TMTAManager : TaskManagerCtr
{
    private static int MAX_NUM_OF_STIMULUS = 10;
    private int _currentTrial = 0;
    private int _currentTrialLimit = 3;
    public Sprite[] _numbers;
    public GameObject[] _numbersObj;
    private int[,] _taskPatterns;

    public float _intervalTime = 1.0f;
    private int _currentOrder = 0;
    private GameObject _taskEndBtn;
    public GameObject _canvas;//キャンバス
    public GameObject _Button;


    //  カウントダウン終了後に呼ばれる
    public override void OnTaskBegin()
    {
        Debug.Log("OnTaskBegin");
        LoadTargets();

        //LoadTaskPatterns();
        //_currentTrialLimit = 3;//3から始めて１個ずつ10まで増える
        //_currentTrial = 0;
        //_currentOrder = 0;
        //DatabaseManager.Instance.maxTrialNum = 8;
        DisplayTaskEndButton();
    }

    //　タスク中に表示される終了ボタンのイベント
    public override void OnTaskEndButton()
    {
        GameObject.Find("UIManager").GetComponent<TextManager>().ChangeEnabled(GameObject.Find("Text").GetComponent<Text>());
        Debug.Log("OnTaskEndButton:タスクが終了しました");
        DeleteTaskEndButton();
        DeleteAllStimulus();
        DisplayGobackToMenuButton();
        GameObject.Find("LineCreator").GetComponent<LineCreator>().ClearLine();
        /**
        DatabaseManager.Instance.EndLog();//データログ停止
        DeleteAllStimulus();
        DeleteTaskEndButton();

        if (_currentTrial != 8)//8タスク
        {
            _currentOrder = 0;
            //すべてのディストラクタ刺激を表示する
            SetAllStimulusForTaskInstruction();
            //順番を追ってターゲット刺激を表示する
            InvokeRepeating("PutTargetStimulus", 1.0f, _intervalTime);
        }
        else
        {
            DisplayGobackToMenuButton();
            DatabaseManager.Instance.EndLogAndExport();//データログ停止 & 保存

        }
    **/
    }

    protected override void DeleteTaskEndButton()
    {

        Debug.Log("DeleteTaskEndButton:TaskEndButtonを消しました");
        GameObject.Find("TaskEndButton").SetActive(false);
    }

    protected override void DisplayGobackToMenuButton()
    {
        Debug.Log("DisplayGobackToMenuButton:GobackToMenuButtonを表示");

        Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();// Canvasコンポーネントを保持

        foreach (Transform child in _canvas.transform)
        {
            Debug.Log(child.name);
            // 子の要素をたどる
            if (child.name == "GobackToMenuButton" || child.name == "StartButton")
            {
                // 指定した名前と一致
                // 表示フラグを設定
                child.gameObject.SetActive(true);
                // おしまい

            }
        }
    }

    //刺激パターンの読み込み
    public override void LoadTaskPatterns()
    {
        Debug.Log("LoadTaskPatterns:タスクパターンをロード");

        StreamReader sr = new StreamReader("./Assets/Resources/SpatialSpan/TaskPattern/taskPattern.txt", System.Text.Encoding.GetEncoding("shift_jis"));
        string fileContents = sr.ReadToEnd();
        sr.Close();

        _taskPatterns = new int[8, 10];

        var lines = fileContents.Split("\n"[0]);
        for (int j = 0; j < lines.Length; j++)
        {
            var patterns = lines[j].Split(","[0]);
            for (int k = 0; k < 10; k++)
            {
                _taskPatterns[j, k] = int.Parse(patterns[k]);
            }

        }
    }

    //刺激画像の読み込み
    protected override void LoadTargets()
    {
        LoadNumbers();

    }

    void LoadNumbers()
    {
        Debug.Log("LoadTargets:ターゲット刺激を読みこむ");
        //spriteの読み込み（multiple）
        _numbers = new Sprite[25];
        _numbersObj = new GameObject[25];


        for (int i = 0; i < 25; i++)
        {
            _numbers[i] = Resources.Load<Sprite>("TMT-A/number"+(i+1).ToString());

            //spriteを表示するためのゲームオブジェクトの配列
            _numbersObj[i] = new GameObject();
            _numbersObj[i].gameObject.name = "number"+(i + 1).ToString();

            _numbersObj[i].transform.position = new Vector3(0f, 2.68f, 0.5f);
            _numbersObj[i].AddComponent<SpriteRenderer>();
            _numbersObj[i].GetComponent<SpriteRenderer>().sprite = _numbers[i];

        }
        _numbersObj[0].transform.position = new Vector3(-0.39f, 1.27f, 0.5f);
        _numbersObj[1].transform.position = new Vector3(1.39f, -1.16f, 0.5f);
        _numbersObj[2].transform.position = new Vector3(1.95f, 2.74f, 0.5f);
        _numbersObj[3].transform.position = new Vector3(-3.08f, 1.9f, 0.5f);
        _numbersObj[4].transform.position = new Vector3(-2.42f, -1.52f, 0.5f);
        _numbersObj[5].transform.position = new Vector3(-1.26f, 0.24f, 0.5f);
        _numbersObj[6].transform.position = new Vector3(-0.68f, -2.15f, 0.5f);
        _numbersObj[7].transform.position = new Vector3(2.21f, -3.92f, 0.5f);
        _numbersObj[8].transform.position = new Vector3(4.09f, -3.12f, 0.5f);
        _numbersObj[9].transform.position = new Vector3(1.86f, -2.44f, 0.5f);
        _numbersObj[10].transform.position = new Vector3(4.1f, 0.67f, 0.5f);
        _numbersObj[11].transform.position = new Vector3(5.48f, -4.29f, 0.5f);
        _numbersObj[12].transform.position = new Vector3(-1.62f, -3.31f, 0.5f);
        _numbersObj[13].transform.position = new Vector3(-0.6f, -4.4f, 0.5f);
        _numbersObj[14].transform.position = new Vector3(-3.98f, -3.22f, 0.5f);
        _numbersObj[15].transform.position = new Vector3(-3.1f, -2.61f, 0.5f);
        _numbersObj[16].transform.position = new Vector3(-5.12f, -0.6799999f, 0.5f);
        _numbersObj[17].transform.position = new Vector3(-3.7f, -1.1f, 0.5f);
        _numbersObj[18].transform.position = new Vector3(-5.01f, 2.73f, 0.5f);
        _numbersObj[19].transform.position = new Vector3(-5.16f, 1.12f, 0.5f);
        _numbersObj[20].transform.position = new Vector3(-6.24f, 3.35f, 0.5f);
        _numbersObj[21].transform.position = new Vector3(-3.04f, 3.56f, 0.5f);
        _numbersObj[22].transform.position = new Vector3(4.69f, 4.29f, 0.5f);
        _numbersObj[23].transform.position = new Vector3(-0.78f, 2.82f, 0.5f);
        _numbersObj[24].transform.position = new Vector3(3.95f, 2.9f, 0.5f);



        //すべてのディストラクタ刺激を表示する
        //PutAllStimulus();
        //順番を追ってターゲット刺激を表示する
        //InvokeRepeating("PutTargetStimulus", 0.0f, _intervalTime);
    }


    protected override void DisplayTaskEndButton()
    {
        Debug.Log("DisplayTaskEndButton:TaskEndButtonを表示");
        float scrWidth = Screen.width;
        float scrHeight = Screen.height;


        _taskEndBtn = (GameObject)Instantiate(_Button);
        _taskEndBtn.transform.SetParent(_canvas.transform, false);
        _taskEndBtn.gameObject.name = "TaskEndButton";
        _taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(-scrWidth/2 + 160/2 + 30, scrHeight / 2 - 30, 0);
        _taskEndBtn.GetComponent<Button>().onClick.AddListener(OnTaskEndButton);

        //_taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(0,0, 0);
    }//


    void DeleteAllStimulus()
    {
        Debug.Log("DeleteAllStimulus:");

        for(int i = 0; i < 25; i++)
        { 
            _numbersObj[i].gameObject.SetActive(false);
        }
    }






    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            DatabaseManager.Instance.EndLog();//データログ停止
        }
    }

}
