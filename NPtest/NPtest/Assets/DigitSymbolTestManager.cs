﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class DigitSymbolTestManager : TaskManagerCtr
{
    private static int MAX_NUM_OF_STIMULUS = 10;
    private int _currentTrial = 0;
    private int _currentTrialLimit = 3;
    public Sprite[] _stimulus;
    private Sprite _code;
    private GameObject _codeObj;
    private Sprite _answerSheet;
    private GameObject _answerSheetObj;
    private int[,] _taskPatterns;
    public GameObject[] _stimulusObj;
    public float _intervalTime = 1.0f;
    private int _currentOrder = 0;
    private GameObject _taskEndBtn;
    public GameObject _canvas;//キャンバス
    public GameObject _Button;


    //  カウントダウン終了後に呼ばれる
    public override void OnTaskBegin()
    {
        Debug.Log("OnTaskBegin");
        LoadTargets();

        //LoadTaskPatterns();
        //_currentTrialLimit = 3;//3から始めて１個ずつ10まで増える
        //_currentTrial = 0;
        //_currentOrder = 0;
        //DatabaseManager.Instance.maxTrialNum = 8;
        DisplayTaskEndButton();
    }

    //　タスク中に表示される終了ボタンのイベント
    public override void OnTaskEndButton()
    {
        GameObject.Find("UIManager").GetComponent<TextManager>().ChangeEnabled(GameObject.Find("Text").GetComponent<Text>());
        Debug.Log("OnTaskEndButton:タスクが終了しました");
        DeleteTaskEndButton();
        DeleteAllStimulus();
        DisplayGobackToMenuButton();
        GameObject.Find("LineCreator").GetComponent<LineCreator>().ClearLine();
        /**
        DatabaseManager.Instance.EndLog();//データログ停止
        DeleteAllStimulus();
        DeleteTaskEndButton();

        if (_currentTrial != 8)//8タスク
        {
            _currentOrder = 0;
            //すべてのディストラクタ刺激を表示する
            SetAllStimulusForTaskInstruction();
            //順番を追ってターゲット刺激を表示する
            InvokeRepeating("PutTargetStimulus", 1.0f, _intervalTime);
        }
        else
        {
            DisplayGobackToMenuButton();
            DatabaseManager.Instance.EndLogAndExport();//データログ停止 & 保存

        }
    **/
    }

    protected override void DeleteTaskEndButton()
    {

        Debug.Log("DeleteTaskEndButton:TaskEndButtonを消しました");
        GameObject.Find("TaskEndButton").SetActive(false);
    }

    protected override void DisplayGobackToMenuButton()
    {
        Debug.Log("DisplayGobackToMenuButton:GobackToMenuButtonを表示");

        Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();// Canvasコンポーネントを保持

        foreach (Transform child in _canvas.transform)
        {
            Debug.Log(child.name);
            // 子の要素をたどる
            if (child.name == "GobackToMenuButton" || child.name == "StartButton")
            {
                // 指定した名前と一致
                // 表示フラグを設定
                child.gameObject.SetActive(true);
                // おしまい

            }
        }
    }

    //刺激パターンの読み込み
    public override void LoadTaskPatterns()
    {
        Debug.Log("LoadTaskPatterns:タスクパターンをロード");

        //StreamReader sr = new StreamReader("./Assets/Resources/SpatialSpan/TaskPattern/taskPattern.txt", System.Text.Encoding.GetEncoding("shift_jis"));
        StreamReader sr = new StreamReader("./TaskPattern/taskPattern.txt", System.Text.Encoding.GetEncoding("shift_jis"));
        string fileContents = sr.ReadToEnd();
        sr.Close();

        _taskPatterns = new int[8, 10];

        var lines = fileContents.Split("\n"[0]);
        for (int j = 0; j < lines.Length; j++)
        {
            var patterns = lines[j].Split(","[0]);
            for (int k = 0; k < 10; k++)
            {
                _taskPatterns[j, k] = int.Parse(patterns[k]);
            }

        }
    }

    //刺激画像の読み込み
    protected override void LoadTargets()
    {
        LoadCode();
        LoadAnswerSheet();
    }

    void LoadCode()
    {
        Debug.Log("LoadTargets:ターゲット刺激を読みこむ");
        //spriteの読み込み（multiple）
        _code = Resources.Load<Sprite>("DigitSymbolTest/code");

        //spriteを表示するためのゲームオブジェクトの配列
        _codeObj = new GameObject();
        _codeObj.gameObject.name = "code";

        _codeObj.transform.position = new Vector3(0f, 2.68f, 0.5f);
        _codeObj.AddComponent<SpriteRenderer>();
        _codeObj.GetComponent<SpriteRenderer>().sprite = _code;
        //_codeObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //_codeObj.AddComponent<CircleCollider2D>();
        //_codeObj.AddComponent<StimulusManager>();


        //すべてのディストラクタ刺激を表示する
        //PutAllStimulus();
        //順番を追ってターゲット刺激を表示する
        //InvokeRepeating("PutTargetStimulus", 0.0f, _intervalTime);
    }

    void LoadAnswerSheet()
    {
        Debug.Log("LoadTargets:ターゲット刺激を読みこむ");
        //spriteの読み込み（multiple）
        _answerSheet = Resources.Load<Sprite>("DigitSymbolTest/answerSheet");

        //spriteを表示するためのゲームオブジェクトの配列
        _answerSheetObj = new GameObject();
        _answerSheetObj.gameObject.name = "answerSheet";

        _answerSheetObj.transform.position = new Vector3(0f, -1.07f, 0.5f);
        _answerSheetObj.AddComponent<SpriteRenderer>();
        _answerSheetObj.GetComponent<SpriteRenderer>().sprite = _answerSheet;
        //_codeObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //_codeObj.AddComponent<CircleCollider2D>();
        //_codeObj.AddComponent<StimulusManager>();


        //すべてのディストラクタ刺激を表示する
        //PutAllStimulus();
        //順番を追ってターゲット刺激を表示する
        //InvokeRepeating("PutTargetStimulus", 0.0f, _intervalTime);
    }

    protected override void DisplayTaskEndButton()
    {
        Debug.Log("DisplayTaskEndButton:TaskEndButtonを表示");
        float scrWidth = Screen.width;
        float scrHeight = Screen.height;


        _taskEndBtn = (GameObject)Instantiate(_Button);
        _taskEndBtn.transform.SetParent(_canvas.transform, false);
        _taskEndBtn.gameObject.name = "TaskEndButton";
        _taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(0, -scrHeight / 2 + 30, 0);
        _taskEndBtn.GetComponent<Button>().onClick.AddListener(OnTaskEndButton);
        
        //_taskEndBtn.GetComponent<RectTransform>().localPosition = new Vector3(0,0, 0);
    }//

    void PutTargetStimulus()
    {
        Debug.Log("PutTargetStimulus:刺激を表示");
        //MAX_NUM_OF_STIMULUS数表示されたら表示をやめる
        if (_currentOrder == _currentTrialLimit)
        {
            _currentTrial++; DatabaseManager.Instance.trialNum++;
            _currentTrialLimit++;
            CancelInvoke("PutTargetStimulus");
            DeleteAllStimulus();
            Invoke("SetAllStimulusForTask", 1.0f);//タスク用の刺激を表示する

            return;
        }


        int targetStimuNum = _taskPatterns[_currentTrial, _currentOrder];

        _stimulusObj[targetStimuNum].GetComponent<SpriteRenderer>().sprite = _stimulus[1];
        //_stimulusObj[_currentOrder].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        //ターゲット以外は白にする
        for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
        {
            if (i != targetStimuNum)
            {
                //_stimulusObj[i].AddComponent<SpriteRenderer>();
                _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
                _stimulusObj[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
        }

        _currentOrder++;

    }

    void PutAllStimulus()
    {
        Debug.Log("PutAllStimulus:");

        if (_stimulusObj[0].GetComponent<SpriteRenderer>() == null)
        {
            for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
            {
                _stimulusObj[i].AddComponent<SpriteRenderer>();
                _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
                _stimulusObj[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                _stimulusObj[i].AddComponent<CircleCollider2D>();
                _stimulusObj[i].AddComponent<StimulusManager>();

            }
        }

    }

    void DeleteAllStimulus()
    {
        Debug.Log("DeleteAllStimulus:");

        _codeObj.gameObject.SetActive(false);
        _answerSheetObj.gameObject.SetActive(false);

    }

    void SetAllStimulusForTaskInstruction()
    {
        Debug.Log("SetAllStimulusForTaskInstruction:");

        for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
        {
            _stimulusObj[i].gameObject.SetActive(true);
            _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
        }
        Debug.Log("トライアルスタート");

    }

    void SetAllStimulusForTask()
    {
        Debug.Log("SetAllStimulusForTask:");

        for (int i = 0; i < MAX_NUM_OF_STIMULUS; i++)
        {
            _stimulusObj[i].gameObject.SetActive(true);
            _stimulusObj[i].GetComponent<SpriteRenderer>().sprite = _stimulus[0];
        }
        Debug.Log("トライアルスタート");
        DisplayTaskEndButton();//taskEndButtonの表示
        DatabaseManager.Instance.cTaskStatus = CURRENT_TASK_STATUS.SPATIAL_SPAN;
        DatabaseManager.Instance.StartLog();//データログ開始
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            DatabaseManager.Instance.EndLog();//データログ停止
        }
    }

}
